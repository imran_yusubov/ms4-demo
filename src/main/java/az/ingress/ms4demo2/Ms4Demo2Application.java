package az.ingress.ms4demo2;

import az.ingress.ms4demo2.model.Address;
import az.ingress.ms4demo2.model.Project;
import az.ingress.ms4demo2.model.Student;
import az.ingress.ms4demo2.repository.AddressRepository;
import az.ingress.ms4demo2.repository.ProjectRepository;
import az.ingress.ms4demo2.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@SpringBootApplication
public class Ms4Demo2Application implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final AddressRepository addressRepository;
    private final ProjectRepository projectRepository;

    public static void main(String[] args) {
        SpringApplication.run(Ms4Demo2Application.class, args);
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {
        //**********NEW or Transient*********//
//
        Project project = new Project();
        project.setName("LMS");

        Address address = new Address();
        address.setAddress("R Behbudov 22 A");
        //addressRepository.save(address);

        Student student = new Student();
        student.setName("Raheddin");
        student.setSurname("Test");
        student.setAge(20L);
        //  student.setAddress(address);
        student.setProject(project);

        Student student2 = new Student();
        student2.setName("Mammadali");
        student2.setSurname("Test");
        student2.setAge(20L);
        //   student2.setAddress(address);
        student2.setProject(project);


        Student student3 = new Student();
        student3.setName("Mammadali");
        student3.setSurname("Test");
        student3.setAge(20L);
        //  student3.setAddress(address);
        student3.setProject(project);


        //**********************************//
//        Student studentSaved = studentRepository.save(student); //Detached close
//        Student studentSaved2 = studentRepository.save(student2);
//        Student studentSaved3 = studentRepository.save(student3);


        // studentRepository.deleteById(40L);
        projectRepository.deleteById(29L);
    }
}
