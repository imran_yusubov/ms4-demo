package az.ingress.ms4demo2.controller;

import az.ingress.ms4demo2.service.TicTacToe;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/game")
@RequiredArgsConstructor
public class TikTakToeController {

    private final TicTacToe game;

    @PostMapping("/{x}/{y}")
    public String play(@RequestParam int x, @RequestParam int y) {
        return game.play(x, y);
    }
}
