package az.ingress.ms4demo2.service;

import java.util.List;

public interface ITicTacToeDAO {

    TicTacToeBean getLastMove();

    void saveMove(TicTacToeBean ticTacToeBean);

    TicTacToeBean getLastMove(List a);
}
