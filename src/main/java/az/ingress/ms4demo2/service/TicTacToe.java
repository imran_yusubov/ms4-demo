package az.ingress.ms4demo2.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TicTacToe {

    private Character[][] board = {{'\0', '\0', '\0'}, {'\0', '\0', '\0'}, {'\0', '\0', '\0'}};
    private Character lastPlayer = '\0';

    private final ITicTacToeDAO dao;

    public String play(int x, int y) {
        checkAxis(x, 'X');
        checkAxis(y, 'Y');
        lastPlayer = nextPlayer();
        setBoard(x, y);
        saveMove(x, y);
        if (isWin()) {
            return lastPlayer + " is the winner";
        } else if (isDraw()) {
            return "The result is draw";
        } else {
            return "No winner";
        }
    }

    private boolean isWin() {
        int playerTotal = 3 * lastPlayer;

        if (checkDioganals(playerTotal) || checkHandVStates(playerTotal))
            return true;
        else
            return false;
    }

    private boolean checkHandVStates(int playerTotal) {
        for (int i = 0; i < board.length; i++) {
            int horizontal = board[0][i] + board[1][i] + board[2][i];
            int vertical = board[i][0] + board[i][1] + board[i][2];
            System.out.println("v=" + vertical + "  h=" + horizontal + " pt=" + playerTotal);
            if (horizontal == playerTotal || vertical == playerTotal) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDioganals(int playerTotal) {
        int dioganal1 = board[0][0] + board[1][1] + board[2][2];
        int dioganal2 = board[0][2] + board[1][1] + board[2][0];
        if (dioganal1 == playerTotal || dioganal2 == playerTotal)
            return true;
        else
            return false;
    }

    private void setBoard(int x, int y) {
        if (board[x - 1][y - 1] != '\0') {
            throw new RuntimeException("Box is occupied");
        } else {
            board[x - 1][y - 1] = lastPlayer;
        }
    }

    private void checkAxis(int axis, Character axisName) {
        if (axis > 3 || axis < 1) {
            throw new RuntimeException(axisName + " outside of the board");
        }
    }

    public char nextPlayer() {
        if (lastPlayer == 'X') {
            return 'O';
        }
        return 'X';
    }

    private boolean isDraw() {
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board.length; y++) {
                if (board[x][y] == '\0') {
                    return false;
                }
            }
        }
        return true;
    }


    private void saveMove(int x, int y) {
       // TicTacToeBean lastMove = dao.getLastMove();
        TicTacToeBean lastMove = dao.getLastMove(List.of(1, 2, 4));
        dao.saveMove(new TicTacToeBean(lastMove.getMove() + 1, lastPlayer, x, y));
        dao.saveMove(new TicTacToeBean(lastMove.getMove() + 1, lastPlayer, x, y));
    }

}


