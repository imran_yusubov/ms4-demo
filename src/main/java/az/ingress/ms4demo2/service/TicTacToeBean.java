package az.ingress.ms4demo2.service;


public final class TicTacToeBean {

    private Integer move;
    private Character player;
    private Integer x;
    private Integer y;

    public TicTacToeBean(Integer move, Character player, Integer x, Integer y) {
        this.move = move;
        this.player = player;
        this.x = x;
        this.y = y;
    }

    public Integer getMove() {
        return move;
    }

    public void setMove(Integer move) {
        this.move = move;
    }

    public Character getPlayer() {
        return player;
    }

    public void setPlayer(Character player) {
        this.player = player;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof  TicTacToeBean && obj.getClass().equals(TicTacToeBean.class)){
            return (move==((TicTacToeBean) obj).move &&
                    player==((TicTacToeBean) obj).player&&
                    x==((TicTacToeBean) obj).getX()&&
                    y==((TicTacToeBean) obj).getY());
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb=new StringBuilder();
        sb.append("Move :"+move+"\n");
        sb.append("Player :"+player+"\n");
        sb.append("X :"+x+"\n");
        sb.append("Y :"+y+"\n");
        return sb.toString();
    }
}
