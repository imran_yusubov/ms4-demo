package az.ingress.ms4demo2.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TicTacToeDAO implements ITicTacToeDAO {

    @Override
    public TicTacToeBean getLastMove() {
        log.info("This a real implementation");
        return new TicTacToeBean(1, 'X', 1, 2);
    }

    @Override
    public void saveMove(TicTacToeBean ticTacToeBean) {
        //save
        log.info("This a real implementation");
    }

    @Override
    public TicTacToeBean getLastMove(List a) {
        return new TicTacToeBean(1, 'X', 1, 2);
    }
}
