package az.ingress.ms4demo2.repository;

import az.ingress.ms4demo2.model.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Long> {

}
