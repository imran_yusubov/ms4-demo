package az.ingress.ms4demo2.repository;

import az.ingress.ms4demo2.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
