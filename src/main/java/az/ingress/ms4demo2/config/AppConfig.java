package az.ingress.ms4demo2.config;

import az.ingress.ms4demo2.model.Student;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    public String name;

    public String version;

    public List<String> developers;

    public Map<String, Student> tags;
}
