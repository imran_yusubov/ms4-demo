package az.ingress.ms4demo2.model;

import lombok.Builder;

@Builder
public class StudentDto {

    private Long id;

    private String name;

    private String lastName;

    private String age;

    private String address;
}
