package az.ingress.ms4demo2.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/*
   public method
 */
class CalculatorTest {

    private Calculator calculator = new Calculator();

    //Supports add operation
    //As a user, when I enter 6 and 5 as input, the result should be 11
    //AAA style : Arrange, Act, Assert
    @Test
    public void givenAAndBWhenAddThenC() {
        //Arrange
        int a = 5;
        int b = 6;
        int c = 11;

        //Act
        int result = calculator.add(a, b);

        //Assert
        assertThat(result).isEqualTo(c);
    }

    //Supports subtract operation
    //As a user, when I enter 6 and 5 as input, the result should be 1
    @Test
    public void givenAAndBWhenSubtractThenC() {
        int a = 5;
        int b = 6;
        int c = 1;

        int result = calculator.subtract(a, b);

        assertThat(result).isEqualTo(c);
    }

}