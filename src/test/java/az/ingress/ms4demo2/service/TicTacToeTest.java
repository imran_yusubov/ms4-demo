package az.ingress.ms4demo2.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TicTacToeTest {

    @Captor
    ArgumentCaptor<TicTacToeBean> ticTacToeBeanArgumentCaptor;

    @Spy
    private ITicTacToeDAO dao;

    @InjectMocks
    private TicTacToe ticTacToe;

    @BeforeEach
    public void initSomething() {
        System.out.println("Before each");
    }


    @Test
    public void whenXOutsideBoardThenRuntimeException() {
        //Arrange
        int x = 5; //is outside of the board
        int y = 2;

        //Act & Arrange
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X outside of the board");
    }


    @Test
    public void whenYOutsideBoardThenRuntimeException() {
        //Arrange
        int x = 2;
        int y = 5; //is outside of the board

        //Act & Arrange
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y outside of the board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        //Arrange
        int x = 2;
        int y = 1;
        when(dao.getLastMove(List.of(1, 2, 4))).thenReturn(new TicTacToeBean(1, 'X', 1, 2));

        ticTacToe.play(x, y);

        //Act & Arrange
        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO() {
        //Arrange
        when(dao.getLastMove(List.of(1, 2, 4))).thenReturn(new TicTacToeBean(1, 'X', 1, 2));

        //Act
        ticTacToe.play(1, 1); //Player is X

        //Assert
        assertThat(ticTacToe.nextPlayer()).isEqualTo('O');
        //verify(dao, times(2)).saveMove(any());
        verify(dao, times(2)).saveMove(ticTacToeBeanArgumentCaptor.capture());

        TicTacToeBean value = ticTacToeBeanArgumentCaptor.getValue();
        assertThat(value).isEqualTo(new TicTacToeBean(2, 'X', 1, 1));
    }

    @Test
    public void whenPlayThenNoWinner() {
        when(dao.getLastMove(List.of(1, 2, 4))).thenReturn(new TicTacToeBean(1, 'X', 1, 2));

        String actual = ticTacToe.play(1, 1);
        assertThat(actual).isEqualTo("No winner"); //O
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        when(dao.getLastMove(List.of(1, 2, 4))).thenReturn(new TicTacToeBean(1, 'X', 1, 2));

        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O

        String actual = ticTacToe.play(3, 1); // X

        assertThat(actual).isEqualTo("X is the winner"); //O
    }

    /****-------------Added after *****************/
    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        when(dao.getLastMove(List.of(1, 2, 4))).thenReturn(new TicTacToeBean(1, 'X', 1, 2));

        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        String actual = ticTacToe.play(1, 3); // O
        assertThat(actual).isEqualTo("O is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        when(dao.getLastMove(List.of(1, 2, 4))).thenReturn(new TicTacToeBean(1, 'X', 1, 2));

        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
        String actual = ticTacToe.play(3, 2);
        assertThat(actual).isEqualTo("The result is draw");
    }


    @Test
    public void testList() {
        Set<Integer> integers1 = Set.of(1, 2, 4, 3);

        Set<Integer> integers2 = Set.of(1, 2, 3, 4);


        //  assertThat(integers1 == integers2).isEqualTo(true);
        assertThat(integers1.equals(integers2)).isEqualTo(true);


    }

}