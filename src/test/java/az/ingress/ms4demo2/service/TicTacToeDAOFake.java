package az.ingress.ms4demo2.service;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TicTacToeDAOFake implements ITicTacToeDAO {

    TicTacToeBean ticTacToeBean;

    @Override
    public TicTacToeBean getLastMove() {
        System.out.println("This is fake impl getLastMove");
        return new TicTacToeBean(1, 'X', 1, 2);
    }

    @Override
    public void saveMove(TicTacToeBean ticTacToeBean) {
        System.out.println("This is fake impl saveMove");
        this.ticTacToeBean = ticTacToeBean;
    }

    @Override
    public TicTacToeBean getLastMove(List a) {
        return null;
    }

    public TicTacToeBean getArgument() {
        return ticTacToeBean;
    }


}
