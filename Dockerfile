FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
ENTRYPOINT ["java"]
